﻿using cartography.Application.Domain;
using Microsoft.EntityFrameworkCore;

namespace cartography.Data.Context
{
    public class CartographyContext : DbContext
    {
        public CartographyContext(DbContextOptions<CartographyContext> options) : base(options)
        {
        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Sector> Sectors {  get; set;}
        public DbSet<CampaignDate> CampaignDates { get; set; }

        //join tables
        public DbSet<Registration> Registrations { get; set; }

    }
}
