﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cartography.Data.Migrations
{
    public partial class RefactorRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Availability",
                table: "Registrations");

            migrationBuilder.DropColumn(
                name: "FundsRaised",
                table: "Registrations");

            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "Registrations",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<bool>(
                name: "Afternoon",
                table: "Registrations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "FullDay",
                table: "Registrations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Morning",
                table: "Registrations",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Afternoon",
                table: "Registrations");

            migrationBuilder.DropColumn(
                name: "FullDay",
                table: "Registrations");

            migrationBuilder.DropColumn(
                name: "Morning",
                table: "Registrations");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "Registrations",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<int>(
                name: "Availability",
                table: "Registrations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FundsRaised",
                table: "Registrations",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
