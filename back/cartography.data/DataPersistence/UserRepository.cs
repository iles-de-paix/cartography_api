﻿using cartography.Application.Domain;
using cartography.Application.Domain.Exceptions;
using cartography.Application.Services.DataPersistence;
using cartography.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace cartography.Data.DataPersistence
{
    public class UserRepository : IUserRepository
    {
        private readonly CartographyContext _cartographyContext;

        public UserRepository(CartographyContext cartographyContext)
        {
            _cartographyContext = cartographyContext;
        }

        public async Task<List<User>> GetUserAsync()
        {
            return await _cartographyContext.Users.ToListAsync();
        }

        public async Task AddUserAsync(User userToAdd)
        {
            await _cartographyContext.Users.AddAsync(userToAdd);
            await _cartographyContext.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(Guid id)
        {
            var userToDelete = await _cartographyContext.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (userToDelete == null)
                UserException.UserNotFound();

            _cartographyContext.Users.Remove(userToDelete);
            await _cartographyContext.SaveChangesAsync();
        }
    }
}
