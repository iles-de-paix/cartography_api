﻿using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;
using cartography.Data.Context;

namespace cartography.Data.DataPersistence
{
    public class RegistrationRepository : IRegistrationRepository
    {
        private readonly CartographyContext _cartographyContext;

        public RegistrationRepository(CartographyContext cartographyContext)
        {
            _cartographyContext = cartographyContext;
        }

        public async Task AddRegistrationAsync(Registration registration)
        {
            await _cartographyContext.AddAsync(registration);
            await _cartographyContext.SaveChangesAsync();
        }
    }
}
