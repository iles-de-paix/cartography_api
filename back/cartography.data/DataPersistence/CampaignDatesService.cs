﻿using cartography.Application.Domain;
using cartography.Application.Domain.Exceptions;
using cartography.Application.Services.DataPersistence;
using cartography.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace cartography.Data.DataPersistence
{
    public class CampaignDatesService : ICampaignDatesService
    {
        private readonly CartographyContext _cartographyContext;
        public CampaignDatesService(CartographyContext cartographyContext)
        {
            _cartographyContext = cartographyContext;
        }

        public async Task<List<CampaignDate>> GetAsync(int year)
        {
            List<CampaignDate> dates;
            if(year != default)
            {
                dates = await _cartographyContext.CampaignDates.Where(x => x.Date.Year == year).ToListAsync();
            }
            else
            {
                dates = await _cartographyContext.CampaignDates.ToListAsync();
            }
            return dates;
        }
        public async Task AddAsync(CampaignDate date)
        {
            _cartographyContext.CampaignDates.Add(date);
            await _cartographyContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var campaignDate = _cartographyContext.CampaignDates.Where(x => x.Id == id).FirstOrDefault();
            if (campaignDate == null)
            {
                CampaignDatesException.CampaignDateNotFound();
            }

            _cartographyContext.CampaignDates.Remove(campaignDate);
            await _cartographyContext.SaveChangesAsync();
        }
    }
}
