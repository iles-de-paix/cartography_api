﻿using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;
using cartography.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace cartography.Data.DataPersistence
{
    public class LocationRepository : ILocationRepository
    {
        private readonly CartographyContext _cartographyContext;
        public LocationRepository(CartographyContext cartographyContext)
        {
            _cartographyContext = cartographyContext;
        }

        public async Task<List<Location>> GetLocations(int zipCode = default)
        {
            List<Location> locations;
            if(zipCode != default)
            {
                locations = await _cartographyContext.Locations.Where(x => x.ZipCode == zipCode).ToListAsync();
            }
            else
            {
                locations = await _cartographyContext.Locations.ToListAsync();
            }
            return locations;
        }
    }
}
