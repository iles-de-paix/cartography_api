﻿namespace cartography.Application.Domain.Enums
{
    public enum Category
    {
        Person,
        School,
        Enterprise,
        YouthMouvement,
        Other
    }
}
