﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cartography.Application.Domain.Enums
{
    public enum Role
    {
        Volunteer,
        Manager,
        Admin
    }
}
