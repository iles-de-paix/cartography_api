﻿namespace cartography.Application.Domain.Enums
{
    public enum Availability
    {
        Morning,
        Afternoon,
        FullDay
    }
}
