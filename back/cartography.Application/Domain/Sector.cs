﻿namespace cartography.Application.Domain
{
    public class Sector : Base
    {
        public string Name { get; set; } = string.Empty;
        public string Description {  get; set; } = string.Empty;
        public Guid? UserId { get; set; }
        public virtual User? User { get; set; }
        public virtual List<Location> Locations {  get; set; } = new();
    }
}
