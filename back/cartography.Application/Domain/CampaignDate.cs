﻿
namespace cartography.Application.Domain
{
    public class CampaignDate : Base
    {
        public DateTime Date {  get; set; } = DateTime.Now;
    }
}
