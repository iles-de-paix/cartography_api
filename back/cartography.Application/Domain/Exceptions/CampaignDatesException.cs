﻿namespace cartography.Application.Domain.Exceptions
{
    public class CampaignDatesException : Exception
    {
        public CampaignDatesException(string message) : base(message)
        {}

        public static void CampaignDateNotFound()
        {
            throw new CampaignDatesException("Can't find campaign date in Database !");
        }
    }
}
