﻿namespace cartography.Application.Domain.Exceptions
{
    public class UserException : Exception
    {
        public UserException(string message) : base(message) 
        {

        }

        public static void UserNotFound()
        {
            throw new UserException("Can't find user in database");
        }
    }
}
