﻿using cartography.Application.Domain.Enums;

namespace cartography.Application.Domain
{
    public class User : Base
    {
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public Category Category { get; set; }
        public int NumberOfPeople { get; set; }
        public Role Role { get; set; }
        public virtual List<Registration> UserLocations { get; set; } = new();
    }
}
