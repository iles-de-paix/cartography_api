﻿namespace cartography.Application.Domain
{
    public class Location : Base
    {
        public Location(int? streetNumber, string box, string streetName, string streetName2, int zipCode, string locality, string phoneNumber, string municipality, string description)
        {
            StreetNumber = streetNumber;
            Box = box;
            StreetName = streetName;
            StreetName2 = streetName2;
            ZipCode = zipCode;
            Locality = locality;
            PhoneNumber = phoneNumber;
            Municipality = municipality;
            Description = description;
        }

        public Location()
        {}

        public int? StreetNumber { get; set; }
        public string Box { get; set; } = string.Empty;
        public string StreetName { get; set; } = string.Empty;
        public string StreetName2 { get; set; } = string.Empty;
        public int ZipCode { get; set; }
        public string Locality { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string Municipality { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public Guid? SectorId { get; set; }
        public virtual Sector? Sector { get; set; }
        public virtual List<Registration> Users { get; set; } = new();
    }
}
