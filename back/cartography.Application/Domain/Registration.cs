﻿using cartography.Application.Domain.Enums;

namespace cartography.Application.Domain
{
    public class Registration : Base
    {
        public bool Morning { get; set; }
        public bool Afternoon { get; set; }
        public bool FullDay { get; set; }
        public string Date { get; set; } = string.Empty;
        public Guid? LocationId { get; set; }
        public virtual Location? Location { get; set; }
        public Guid? UserId { get; set; }
        public virtual User? User { get; set; }
    }
}
