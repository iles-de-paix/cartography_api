﻿using cartography.Application.Domain;

namespace cartography.Application.Features
{
    public interface ILocationFeature
    {
        Task<List<Location>> GetLocations(int zipCode = default);
    }
}