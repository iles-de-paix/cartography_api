﻿using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;

namespace cartography.Application.Features
{
    public class LocationFeature : ILocationFeature
    {
        private readonly ILocationRepository _locationRepository;
        public LocationFeature(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository ?? throw new ArgumentNullException(nameof(locationRepository));
        }

        public async Task<List<Location>> GetLocations(int zipCode = default)
        {
            return await _locationRepository.GetLocations(zipCode);
        }
    }
}
