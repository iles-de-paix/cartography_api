﻿using cartography.Application.Domain;

namespace cartography.Application.Services.DataPersistence
{
    public interface IRegistrationRepository
    {
        Task AddRegistrationAsync(Registration registration);
    }
}
