﻿using cartography.Application.Domain;

namespace cartography.Application.Services.DataPersistence
{
    public interface IUserRepository
    {
        public Task<List<User>> GetUserAsync();
        public Task AddUserAsync(User userToAdd);
        public Task DeleteUserAsync(Guid id);
    }
}
