﻿using cartography.Application.Domain;

namespace cartography.Application.Services.DataPersistence
{
    public interface ICampaignDatesService
    {
        public Task<List<CampaignDate>> GetAsync(int year);
        public Task AddAsync(CampaignDate date);
        public Task DeleteAsync(Guid id);
    }
}
