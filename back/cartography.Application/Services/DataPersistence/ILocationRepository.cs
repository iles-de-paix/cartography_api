﻿using cartography.Application.Domain;


namespace cartography.Application.Services.DataPersistence
{
    public interface ILocationRepository
    {
        public Task<List<Location>> GetLocations(int zipCode = default);
    }
}
