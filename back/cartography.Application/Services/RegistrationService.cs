﻿using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;

namespace cartography.Application.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IRegistrationRepository _registrationRepository;

        public RegistrationService(IRegistrationRepository registrationRepository)
        {
            _registrationRepository = registrationRepository;
        }

        public async Task AddRegistrationAsync(Registration registration)
        {
            await _registrationRepository.AddRegistrationAsync(registration);
        }
    }
}
