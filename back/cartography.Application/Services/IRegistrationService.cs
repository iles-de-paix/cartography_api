﻿using cartography.Application.Domain;

namespace cartography.Application.Services
{
    public interface IRegistrationService
    {
        Task AddRegistrationAsync(Registration registration);
    }
}
