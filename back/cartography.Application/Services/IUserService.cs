﻿using cartography.Application.Domain;

namespace cartography.Application.Services
{
    public interface IUserService
    {
        public Task<List<User>> GetUsersAsync();
        public Task AddUserAsync(User userToAdd);
        public Task DeleteUserAsync(Guid id);
    }
}
