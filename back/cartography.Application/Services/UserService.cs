﻿using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;

namespace cartography.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task AddUserAsync(User userToAdd)
        {
            await _userRepository.AddUserAsync(userToAdd);
        }

        public async Task DeleteUserAsync(Guid id)
        {
            await _userRepository.DeleteUserAsync(id);
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await _userRepository.GetUserAsync();
        }
    }
}
