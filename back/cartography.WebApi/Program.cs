using cartography.Application.Features;
using cartography.Application.Services;
using cartography.Application.Services.DataPersistence;
using cartography.Data.Context;
using cartography.Data.DataPersistence;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
services.AddMvc();
services.AddSwaggerGen();
services.AddDbContext<CartographyContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("IleDePaix")));
services.AddAutoMapper(typeof(Program));

// Cors
services.AddCors(o => o.AddPolicy("AllowAll", builder =>
{
    builder.AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader();
}));

services.AddControllers();

builder.Host.ConfigureServices(services => { services.AddTransient<ILocationFeature, LocationFeature>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<ILocationRepository, LocationRepository>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<ICampaignDatesService, CampaignDatesService>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<IUserRepository, UserRepository>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<IUserService, UserService>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<IRegistrationRepository, RegistrationRepository>(); });
builder.Host.ConfigureServices(services => { services.AddTransient<IRegistrationService, RegistrationService>(); });


var app = builder.Build();


app.UseDeveloperExceptionPage();

app.UseCors(builder => builder
          .AllowAnyOrigin()
          .AllowAnyMethod()
          .AllowAnyHeader());

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    c.RoutePrefix = string.Empty;
});
   


app.UseHttpsRedirection();

app.UseRouting();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
    endpoints.MapRazorPages();
});


app.Run();