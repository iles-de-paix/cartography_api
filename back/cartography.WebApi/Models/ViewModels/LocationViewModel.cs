﻿namespace cartography.WebApi.Models.ViewModels
{
    public class LocationViewModel
    {
        public Guid Id { get; set; }
        public int? StreetNumber { get; set; }
        public string Box { get; set; } = string.Empty;
        public string StreetName { get; set; } = string.Empty;
        public string StreetName2 { get; set; } = string.Empty;
        public int ZipCode { get; set; }
        public string Locality { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string Municipality { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
