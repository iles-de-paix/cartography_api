﻿using cartography.Application.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace cartography.WebApi.Models.InputModels
{
    public class UserInputModel
    {
        [Required]
        public string FirstName { get; set; } = string.Empty;
        [Required]
        public string LastName { get; set; } = string.Empty;
        [Required]
        [EmailAddress]
        public string Email { get; set; } = string.Empty;
        public Category Category { get; set; }
        public int NumberOfPeople { get; set; }
        public Role Role { get; set; }
    }
}
