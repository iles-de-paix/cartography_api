﻿using System.ComponentModel.DataAnnotations;

namespace cartography.WebApi.Models.InputModels
{
    public class CampaignDateInputModel
    {
        [Required]
        public string Date { get; set; }
    }
}
