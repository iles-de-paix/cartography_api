﻿using System.ComponentModel.DataAnnotations;

namespace cartography.WebApi.Models.InputModels
{
    public class RegistrationInputModel
    {
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        public bool Morning { get; set; }
        public bool Afternoon { get; set; }
        public bool FullDay { get; set; }
        [Required]
        public string Date { get; set; } = string.Empty;
    }
}
