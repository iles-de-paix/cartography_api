﻿using AutoMapper;
using cartography.Application.Domain;
using cartography.Application.Services.DataPersistence;
using cartography.WebApi.Models.InputModels;
using Microsoft.AspNetCore.Mvc;

namespace cartography.WebApi.Controllers
{
    //[ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class CampaignDatesController : ControllerBase
    {
        private readonly ICampaignDatesService _campaignDatesService;
        private readonly IMapper _mapper;

        public CampaignDatesController(ICampaignDatesService campaignDatesService, IMapper mapper)
        {
            _campaignDatesService = campaignDatesService ?? throw new ArgumentNullException(nameof(campaignDatesService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get campagin dates.
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(List<CampaignDate>))]
        public async Task<ActionResult> GetDates(int year = default)
        {
            var dates = await _campaignDatesService.GetAsync(year);
            return Ok(dates);
        }

        /// <summary>
        /// Add campaign date.
        /// </summary>
        /// <param name="Date"></param>
        /// <returns>New added campaign date</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult> AddDate(CampaignDateInputModel campaignDateInputModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values);

            var campaignDate = _mapper.Map<CampaignDate>(campaignDateInputModel);
            await _campaignDatesService.AddAsync(campaignDate);
            return CreatedAtAction(nameof(GetDates), new { Id = campaignDate.Id }, campaignDate);
        }

        /// <summary>
        /// Remove campaign date.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteDate(Guid id)
        {
            await _campaignDatesService.DeleteAsync(id);
            return NoContent();
        }
    }
}
