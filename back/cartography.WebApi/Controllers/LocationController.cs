﻿using AutoMapper;
using cartography.Application.Features;
using cartography.WebApi.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace cartography.WebApi.Controllers
{
    //[ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly ILocationFeature _getLocationFeature;
        private readonly IMapper _mapper;

        public LocationController(ILocationFeature getLocationFeature, IMapper mapper)
        {
            _getLocationFeature = getLocationFeature ?? throw new ArgumentNullException(nameof(getLocationFeature));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get locations.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<LocationViewModel>))]
        public async Task<ActionResult> GetLocations(int zipCode = default)
        {
            return Ok(_mapper.Map<List<LocationViewModel>>(await _getLocationFeature.GetLocations(zipCode)));
        }
    }
}
