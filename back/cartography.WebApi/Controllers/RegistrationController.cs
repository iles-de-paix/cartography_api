﻿using AutoMapper;
using cartography.Application.Domain;
using cartography.Application.Services;
using cartography.WebApi.Models.InputModels;
using Microsoft.AspNetCore.Mvc;

namespace cartography.WebApi.Controllers
{
    //[ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRegistrationService _registrationService;

        public RegistrationController(IMapper mapper, IRegistrationService registrationService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _registrationService = registrationService ?? throw new ArgumentNullException(nameof(registrationService));
        }

        /// <summary>
        /// Add Registration.
        /// </summary>
        /// <param name="userInputModel"></param>
        /// <returns>New added registration</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult> AddDate(RegistrationInputModel registrationInputModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values);

            var registration = _mapper.Map<Registration>(registrationInputModel);
            await _registrationService.AddRegistrationAsync(registration);
            return Ok();
        }
    }
}
