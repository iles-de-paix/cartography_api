﻿using AutoMapper;
using cartography.Application.Domain;
using cartography.Application.Services;
using cartography.WebApi.Models.InputModels;
using cartography.WebApi.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace cartography.WebApi.Controllers
{
    //[ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        /// <summary>
        /// Get users.
        /// </summary>
        /// <returns>List of all users</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<UserViewModel>))]
        public async Task<ActionResult> GetUsers()
        {
            var users = await _userService.GetUsersAsync();
            return Ok(_mapper.Map<List<UserViewModel>>(users));
        }

        /// <summary>
        /// Add user.
        /// </summary>
        /// <param name="Date"></param>
        /// <returns>New added campaign date</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult> AddUser(UserInputModel userInputModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.Values);

            var user = _mapper.Map<User>(userInputModel);
            await _userService.AddUserAsync(user);
            return Created("", user);
        }

        /// <summary>
        /// Remove user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteUser(Guid id)
        {
            await _userService.DeleteUserAsync(id);
            return NoContent();
        }
    }
}
