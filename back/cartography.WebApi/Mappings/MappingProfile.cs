﻿using AutoMapper;
using cartography.WebApi.Models.InputModels;
using cartography.Application.Domain;
using cartography.WebApi.Models.ViewModels;

namespace cartography.WebApi.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CampaignDateInputModel, CampaignDate>()
                .ForMember(dest => dest.Date, opt =>opt.MapFrom(src => DateTime.ParseExact(src.Date, "dd/MM/yyyy", null)));
            CreateMap<UserInputModel, User>();

            CreateMap<User, UserViewModel>();

            CreateMap<RegistrationInputModel, Registration>();

            CreateMap<Location, LocationViewModel>();
        }
    }
}
